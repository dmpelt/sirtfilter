#!/bin/bash

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh

bash miniconda.sh -b -p $HOME/condad
export PATH=$HOME/condad/bin:$PATH
which python
conda config --set always_yes yes --set changeps1 no
conda config --add channels astra-toolbox/label/dev
conda update conda
conda install -c defaults -c conda-forge nomkl numpy astra-toolbox numexpr sphinx sphinx_rtd_theme
pip install recommonmark
conda info -a

python setup.py install

sphinx-apidoc -o docs/ sirtfilter/
cp CHANGELOG.md docs/

cd docs
make html
cd ..
mkdir .public
cp -r docs/_build/html/* .public

bash miniconda.sh -b -p $HOME/condab
export PATH=$HOME/condab/bin:$PATH
which python
conda config --set always_yes yes --set changeps1 no
conda update conda
conda install conda-build
conda info -a

cd conda
conda-build --no-test --python 2.7 ./
conda-build --no-test --python 3.4 ./
conda-build --no-test --python 3.5 ./
conda-build --no-test --python 3.6 ./
conda-build --no-test --python 3.7 ./
cd ..

cp -r $HOME/condab/conda-bld/linux-64 .public/
cp -r $HOME/condab/conda-bld/noarch .public/
mv .public public
