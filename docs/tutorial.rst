Tutorial
========

In general, to compute a SIRT-FBP filter, three arguments are required:

* The number of detector column pixels
* The specific angles for which data was acquired
* The number of iterations of SIRT to approximate

With this information, a SIRT-FBP filter can be computed using:

.. code-block:: python

   import numpy as np
   import sirtfilter
   
   nd = 1024 # number of detector column pixels
   ang = np.linspace(0, np.pi, 512) # acquired angles
   its = 100 # number of SIRT iterations to approximate
   
   filt = sirtfilter.getfilter(nd, ang, its)
   
Since filter computation can take a significant amount of time, especially
for large dataset sizes, computed filters are automatically saved to disk. If
a filter is requested for the same arguments later, the precomputed filter is
simply loaded from disk. You can choose which folder to save computed filters
in (for example to select a global filter database folder) by setting:

.. code-block:: python

   filt = sirtfilter.getfilter(nd, ang, its, filter_dir='/some/path')

Furthermore, filters for different numbers of iterations can be computed in a
single call by using a list of iteration numbers:

.. code-block:: python

   its = [50, 100, 200]
   filts = sirtfilter.getfilter(nd, ang, its, filter_dir='/some/path')
   filts[50] # Filter approximating 50 SIRT iterations
   filts[100] # Filter approximating 100 SIRT iterations
   filts[200] # Filter approximating 200 SIRT iterations

The computed filters can be used in various reconstruction software packages
to compute SIRT-FBP reconstructions. Specific examples for different packages
are shown below.

The ASTRA toolbox
-----------------

.. literalinclude:: ../examples/example_astra.py

ASTRA called through TomoPy
---------------------------

.. literalinclude:: ../examples/example_astra_in_tomopy.py

TomoPy's gridrec
----------------

.. literalinclude:: ../examples/example_tomopy.py
