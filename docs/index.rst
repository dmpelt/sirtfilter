.. SIRT-FILTER documentation master file, created by
   sphinx-quickstart on Fri Jul 29 13:18:18 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SIRT-FILTER's documentation!
=======================================

Python package to compute filters and use SIRT-FBP for tomographic reconstruction, as published in:

* Pelt, D.M., & De Andrade, V. (2017). Improved tomographic reconstruction of large-scale real-world data by filter optimization. Advanced Structural and Chemical Imaging 2: 17. `[link] <http://rdcu.be/niW6>`_
* Pelt, D. M., & Batenburg, K. J. (2015). Accurately approximating algebraic tomographic reconstruction by filtered backprojection. In Proceedings of The 13th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine (pp. 158-161). `[link] <http://oai.cwi.nl/oai/asset/23742/23742D.pdf>`_

.. toctree::
   :maxdepth: 2
   
   installation
   tutorial
   sirtfilter
   CHANGELOG.md
   license
   copyright

SIRT-FILTER |release|, build date: |today|.

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
