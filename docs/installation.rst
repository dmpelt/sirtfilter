Installation
============

Using conda
-----------

The latest version of the SIRT-FILTER code can be installed using (ana)conda,
by supplying the custom SIRT-FILTER conda channel:

.. code-block:: bash
   
   conda install -c http://dmpelt.gitlab.io/sirtfilter/ sirtfilter

Note that the SIRT-FILTER code requires the ASTRA toolbox for filter computation,
so the above command can fail if ASTRA is not installed yet. To install ASTRA with
conda, you can run:

.. code-block:: bash
   
   conda install -c astra-toolbox astra-toolbox

To receive automatic updates of the SIRT-FILTER code in the future, it is possible to add 
the custom channel to the conda channel list by running:

.. code-block:: bash
   
   conda config --add channels http://dmpelt.gitlab.io/sirtfilter/


From source
-----------

To install the SIRT-FILTER code from source, the standard approach for Python
packages can be used. Start by cloning the SIRT-FILTER git repository:

.. code-block:: bash
   
   git clone https://gitlab.com/dmpelt/sirtfilter.git
   cd sirtfilter

Since the SIRT-FILTER code only includes Python code, installation is easy:

.. code-block:: bash
   
   python setup.py install

Note that the ASTRA toolbox is required to run the SIRT-FILTER code after
installation.
