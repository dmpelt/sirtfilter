from setuptools import setup, find_packages
setup(
    name = "SIRT-FILTER",
    version = "1.1.0",
    packages = find_packages(),

    author = "Daniel M Pelt",
    author_email = "dmpelt@lbl.gov",
    description = "Python package to compute and use SIRT-FILTERs for tomographic reconstruction.",
    license = "license.txt",
    url = "https://gitlab.com/dmpelt/sirt-filter",
)
