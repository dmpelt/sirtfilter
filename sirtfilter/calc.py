from __future__ import print_function, division
from . import astra_par
import numpy as np
import numexpr as ne
try:
    from tqdm import tqdm
except ImportError:
    import time
    def tqdm(inp):
        ln = len(inp)
        start = time.time()
        for i in inp:
            if i != 0:
                prc = float(i)/ln
                ct = time.time()-start
                totaltime = int(round(ct/prc))
                tmins = totaltime//60
                thours = tmins//60
                secs = int(round(ct/prc - ct))
                mins = secs//60
                hours = mins//60
                print("{}% done ({:02d}:{:02d}:{:02d} remaining of {:02d}:{:02d}:{:02d})".format(round(100*prc),hours, mins % 60, secs % 60, thours, tmins % 60, totaltime % 60), end='\r')
            if i==ln-1:
                print("")
            yield i

import hashlib
import os.path
import scipy.io as sio

__all__ = ['calcfilter', 'getfilter', 'getfilterfile', 'NotPrecomputedException']

def __conv_to_list(its):
    try:
        return list(its)
    except TypeError:
        return [its,]

def calcfilter(npix, a, its, ss=1, ncore=None, proj_type='strip'):
    """Compute a SIRT-FILTER.

    Args:
        npix (int): number of row/column pixels of reconstruction volume.

        a (numpy.ndarray): 1D list of projection angles.

        its (int or list): number of SIRT iterations to approximate (can supply multiple).

    Kwargs:
        ss (int): amount of supersampling to use. More supersampling is slower to compute, but will lead to more accurate approximation.

        ncore (int): number of cores to use.

    Returns:
        dict. Dictionary with the computed filters for each iteration number.
    """
    if npix%2==0:
        npix+=1
    x = np.zeros((npix, npix), dtype=np.float32)
    xt = np.zeros((npix, npix), dtype=np.float32)
    xs = np.zeros((npix, npix), dtype=np.float32)
    if ncore is None:
        ncore = astra_par.cpu_count
    work = np.zeros((ncore, npix, npix), dtype=np.float32)
    s = np.zeros((a.shape[0], ss*npix), dtype=np.float32)
    fac = 1./ss

    alpha = np.float32(1./(npix*a.shape[0]))
    
    out = {}    
    its = sorted(__conv_to_list(its))
    if len(its)==0:
        return out
    mx = max(its)
    
    xx,yy = np.mgrid[-(npix//2):npix//2+1,-(npix//2):npix//2+1]
    mask = xx**2+yy**2 < npix**2/4

    x[npix//2,npix//2]=1
    for i in tqdm(range(mx)):
        ne.evaluate('xs+x', out=xs)
        s.fill(0)
        work.fill(0)
        #astra_par.parfp(x, s, a, fac=fac, ncore=ncore)
        #astra_par.parbp(xt, s, a, fac=fac, ncore=ncore, work=work)
        astra_par.parfpbp(x, xt, s, a, fac=fac, ncore=ncore, work=work, proj_type=proj_type)
        ne.evaluate('where(mask,x - alpha*xt,0)', out=x)
        if (i+1) in its:
            fout = np.zeros((a.shape[0], npix), dtype=np.float32)
            astra_par.parfp(xs, fout, a, ncore=ncore, proj_type=proj_type)
            ne.evaluate('alpha*fout',out=fout)
            out[i+1] = (fout, xs.copy())
    return out

def getfilterfile(npix, a, its, ss=1, filter_dir='./', proj_type='strip', ncore=None, error_if_not_precomputed=False):
    """Compute a SIRT-FILTER and return file name with the stored filter. If filter
    was already computed before, it will return the corresponding file name without
    recomputing.

    Args:
        npix (int): number of row/column pixels of reconstruction volume.

        a (numpy.ndarray): 1D list of projection angles.

        its (int or list): number of SIRT iterations to approximate (can supply multiple).

    Kwargs:
        ss (int): amount of supersampling to use. More supersampling is slower to compute, but will lead to more accurate approximation.

        ncore (int): number of cores to use.

        filter_dir (str): directory to store/load computed filters.

        error_if_not_precomputed (bool): If True, raise exception when a precomputed filter is not found.

    Returns:
        str. File name of the computed filter. If multiple iterations are supplied, returns dictionary with the file names for each iteration number.
    """
    if npix % 2 == 0:
        npix += 1
    its = sorted(__conv_to_list(its))
    tocalc = []
    out = {}
    for i in its:
        fstr = "{} {} {} {} ".format(proj_type, npix, i, ss)
        fstr = fstr + np.array_str(a)
        try:
            fname = hashlib.md5(fstr).hexdigest()
        except TypeError:
            fname = hashlib.md5(fstr.encode('utf-8')).hexdigest()
        out[i] = os.path.join(filter_dir, fname+'.mat')
        if not os.path.exists(out[i]):
            tocalc.append(i)
    if error_if_not_precomputed and len(tocalc)>0:
        raise NotPrecomputedException("Requested filter(s) not precomputed.")
    calcs = calcfilter(npix, a, tocalc, ss=ss, ncore=ncore, proj_type=proj_type)
    for i, filts in calcs.items():
        sio.savemat(out[i], {'filt': filts[0], 'imfilt': filts[1]}, do_compression=True)
    if len(out)==1:
        return out[its[0]]
    return out


def getfilter(npix, a, its, ss=1, filter_dir='./', proj_type='strip', ncore=None, error_if_not_precomputed=False):
    """Compute a SIRT-FILTER. Will load a filter from disk if it was already
    computed before, and save filters after computing them.

    Args:
        npix (int): number of row/column pixels of reconstruction volume.

        a (numpy.ndarray): 1D list of projection angles.

        its (int or list): number of SIRT iterations to approximate (can supply multiple).

    Kwargs:
        ss (int): amount of supersampling to use. More supersampling is slower to compute, but will lead to more accurate approximation.

        ncore (int): number of cores to use.

        filter_dir (str): directory to store/load computed filters.

        error_if_not_precomputed (bool): If True, raise exception when a precomputed filter is not found.

    Returns:
        numpy.ndarray. Computed or loaded filter. If multiple iterations are supplied, returns dictionary with the computed or loaded filters for each iteration number.
    """
    fls = getfilterfile(npix, a, its, ss, filter_dir, proj_type, ncore=ncore, error_if_not_precomputed=error_if_not_precomputed)
    if not isinstance(fls, dict):
        return sio.loadmat(fls)['filt']
    out = {}
    for i, fl in fls.items():
        out[i] = sio.loadmat(fl)['filt']
    return out

class NotPrecomputedException(Exception):
    pass
