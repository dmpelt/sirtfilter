import astra
import numpy as np
import multiprocessing as mp
import concurrent.futures as cf
import numexpr as ne

cpu_count = mp.cpu_count()

__all__ = ['parfp', 'parbp', 'parfpbp']

def parfp(x, s, a, fac=1., ncore=None, proj_type='strip'):
    """Calculate a forward projection using the ASTRA toolbox, in parallel.
    
    Args:
        x (numpy.ndarray): 2D input volume.
        
        s (numpy.ndarray): output sinogram.
        
        a (numpy.ndarray): 1D list of projection angles.
    
    Kwargs:
        fac (float): size of detector pixel relative to volume pixel.
        
        ncore (int): number of cores to use.
        
        proj_type (str): ASTRA projector type.
    """
    vg = astra.create_vol_geom(x.shape)
    if ncore == None:
        ncore = cpu_count
    chnks = np.round(np.linspace(0,s.shape[0],ncore+1)).astype(np.int)
    pids = []
    alg_ids = []
    sids = []
    vid = astra.data2d.link('-vol',vg,x)
    cfg = astra.astra_dict('FP')
    cfg['VolumeDataId']=vid
    for i in range(ncore):
        l = chnks[i]
        r = chnks[i+1]
        pg = astra.create_proj_geom('parallel', fac, s.shape[1], a[l:r])
        pid = astra.create_projector(proj_type,pg,vg)
        pids.append(pid)
        sid = astra.data2d.link('-sino',pg,s[l:r])
        sids.append(sid)
        cfg['ProjectionDataId']=sid
        cfg['ProjectorId']=pid
        alg_id = astra.algorithm.create(cfg)
        alg_ids.append(alg_id)
    e = cf.ThreadPoolExecutor(max_workers=ncore)
    thrds = [e.submit(astra.algorithm_c.run, i) for i in alg_ids]
    for t in thrds:
        t.result()
    astra.algorithm.delete(alg_ids)
    astra.data2d.delete(sids)
    astra.data2d.delete(vid)
    astra.projector.delete(pids)

def parbp(x, s, a, fac=1., ncore=None, proj_type='strip', work=None):
    """Calculate a backprojection using the ASTRA toolbox, in parallel.
    
    Args:
        x (numpy.ndarray): 2D output volume.
        
        s (numpy.ndarray): input sinogram.
        
        a (numpy.ndarray): 1D list of projection angles.
    
    Kwargs:
        fac (float): size of detector pixel relative to volume pixel.
        
        ncore (int): number of cores to use.
        
        proj_type (str): ASTRA projector type.
        
        work (numpy.ndarray): temporary array of size ncore x rows x cols of x. If not specified, a temporary array is created, which will negatively impact performance.
    """
    vg = astra.create_vol_geom(x.shape)
    if ncore is None:
        ncore = cpu_count
    if work is None:
        work = np.empty((ncore, x.shape[0], x.shape[1]), dtype=np.float32)
    chnks = np.round(np.linspace(0,s.shape[0],ncore+1)).astype(np.int)
    pids = []
    alg_ids = []
    sids = []
    vids = []
    cfg = astra.astra_dict('BP')
    for i in range(ncore):
        l = chnks[i]
        r = chnks[i+1]
        pg = astra.create_proj_geom('parallel', fac, s.shape[1], a[l:r])
        pid = astra.create_projector(proj_type,pg,vg)
        pids.append(pid)
        sid = astra.data2d.link('-sino',pg,s[l:r])
        sids.append(sid)
        vid = astra.data2d.link('-vol',vg,work[i])
        vids.append(vid)
        cfg['ReconstructionDataId']=vid
        cfg['ProjectionDataId']=sid
        cfg['ProjectorId']=pid
        alg_id = astra.algorithm.create(cfg)
        alg_ids.append(alg_id)
    e = cf.ThreadPoolExecutor(max_workers=ncore)
    thrds = [e.submit(astra.algorithm_c.run, i) for i in alg_ids]
    for t in thrds:
        t.result()
    astra.algorithm.delete(alg_ids)
    astra.data2d.delete(sids)
    astra.data2d.delete(vids)
    astra.projector.delete(pids)
    if ncore>1:
        ne.evaluate('sum(work, 0)', out=x)
    else:
        x[:]=work

def __run_fp_and_bp(algids):
    astra.algorithm_c.run(algids[0])
    astra.algorithm_c.run(algids[1])
    
def parfpbp(x, xout, s, a, fac=1., ncore=None, proj_type='strip', work=None):
    """Calculate a forward projection followed by a backprojection using the
    ASTRA toolbox, in parallel.
    
    Args:
        x (numpy.ndarray): 2D output volume.
        
        xout (numpy.ndarray): 2D output volume.
        
        s (numpy.ndarray): temporary sinogram.
        
        a (numpy.ndarray): 1D list of projection angles.
    
    Kwargs:
        fac (float): size of detector pixel relative to volume pixel.
        
        ncore (int): number of cores to use.
        
        proj_type (str): ASTRA projector type.
        
        work (numpy.ndarray): temporary array of size ncore x rows x cols of x. If not specified, a temporary array is created, which will negatively impact performance.
    """
    vg = astra.create_vol_geom(x.shape)
    if ncore == None:
        ncore = cpu_count
    if work is None:
        work = np.empty((ncore, x.shape[0], x.shape[1]), dtype=np.float32)
    chnks = np.round(np.linspace(0,s.shape[0],ncore+1)).astype(np.int)
    pids = []
    alg_ids = []
    sids = []
    vids = []
    vid = astra.data2d.link('-vol',vg,x)
    cfg = astra.astra_dict('FP')
    cfg['VolumeDataId']=vid
    cfg2 = astra.astra_dict('BP')
    for i in range(ncore):
        l = chnks[i]
        r = chnks[i+1]
        pg = astra.create_proj_geom('parallel', fac, s.shape[1], a[l:r])
        pid = astra.create_projector(proj_type,pg,vg)
        pids.append(pid)
        sid = astra.data2d.link('-sino',pg,s[l:r])
        sids.append(sid)
        vid2 = astra.data2d.link('-vol',vg,work[i])
        vids.append(vid2)
        cfg['ProjectionDataId']=sid
        cfg2['ProjectionDataId']=sid
        cfg['ProjectorId']=pid
        cfg2['ProjectorId']=pid
        cfg2['ReconstructionDataId']=vid2
        alg_id = astra.algorithm.create(cfg)
        alg_id2 = astra.algorithm.create(cfg2)
        alg_ids.append((alg_id,alg_id2))
    e = cf.ThreadPoolExecutor(max_workers=ncore)
    thrds = [e.submit(__run_fp_and_bp, i) for i in alg_ids]
    for t in thrds:
        t.result()
    for alg in alg_ids:
        astra.algorithm.delete(alg)
    astra.data2d.delete(sids)
    astra.data2d.delete(vid)
    astra.data2d.delete(vids)
    astra.projector.delete(pids)
    if ncore>1:
        ne.evaluate('sum(work, 0)', out=xout)
    else:
        x[:]=work