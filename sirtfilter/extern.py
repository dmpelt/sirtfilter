from __future__ import division
import numpy as np
import astra
import scipy.io as sio
import scipy.signal as ss
from .calc import getfilterfile

__all__ = ['convert_to_tomopy_filter', 'astra_plugin', 'convert_to_slicerecon_filter']


def convert_to_tomopy_filter(f, npix):
    """Convert a computed SIRT-FILTER to an array to use in TomoPy gridrec.

    Args:
        f (numpy.ndarray): computed filter.
        
        npix (int): number of column pixels in detector used in TomoPy.

    Returns:
        np.ndarray. The converted filter in Fourier domain.
    """
    sz = 16
    while sz<npix:
        sz*=2
    sz+=1
    hf = sz//2
    hf2 = f.shape[1]//2
    pad = np.zeros((f.shape[0], sz), dtype=np.float32)
    pad[:, hf - hf2:hf - hf2 + f.shape[1]] = f
    pad = np.fft.ifftshift(pad,1)
    return 2*f.shape[0]*np.fft.fft(pad,axis=1).real[:,:hf]/np.pi

def convert_to_slicerecon_filter(f, filename):
    """Convert a computed SIRT-FILTER to an array to use in SliceRecon / RECAST3D.

    Args:
        f (numpy.ndarray): computed filter.
        
        filename (string): file to store filter in.
    """
    filt_out = np.zeros((f.shape[0], f.shape[1]-1), dtype=np.float32)
    for i in range(f.shape[0]):
        filt_out[i] = np.fft.fft(np.fft.ifftshift(f[i]))[:-1].real
    filt_out.tofile(filename)


class astra_plugin(astra.plugin.ReconstructionAlgorithm2D):
    """Reconstruct using the SIRT-FBP method, as published in:

    * Pelt, D.M., & De Andrade, V. (2017). Improved tomographic reconstruction of 
      large-scale real-world data by filter optimization. Advanced Structural and 
      Chemical Imaging 2: 17. (http://rdcu.be/niW6)
    * Pelt, D. M., & Batenburg, K. J. (2015). Accurately approximating algebraic 
      tomographic reconstruction by filtered backprojection. In Proceedings of The 
      13th International Meeting on Fully Three-Dimensional Image Reconstruction 
      in Radiology and Nuclear Medicine (pp. 158-161). 
      (http://oai.cwi.nl/oai/asset/23742/23742D.pdf)
    
    Args:
        filter_file (str): The file name with the stored filter.
    """

    # The astra_name variable defines the name to use to
    # call the plugin from ASTRA
    astra_name = "SIRT-FBP"

    def initialize(self, cfg, filter_file):
        self.W = astra.OpTomo(self.pid)
        self.f = sio.loadmat(filter_file)['filt']

    def run(self, its):
        sf = np.zeros_like(self.s)
        for i in range(sf.shape[0]):
            sf[i] = ss.fftconvolve(self.s[i], self.f[i], 'same')
        self.v[:] = (self.W.T*sf).reshape(self.W.vshape)

try:
    from savu.plugins.reconstructions.base_astra_recon import BaseAstraRecon as savu_BaseAstraRecon
    from savu.plugins.driver.cpu_plugin import CpuPlugin as savu_CpuPlugin
    from savu.plugins.driver.gpu_plugin import GpuPlugin as savu_GpuPlugin
    from savu.data.plugin_list import CitationInformation as savu_CitationInformation
    from savu.plugins.utils import register_plugin as savu_register_plugin


    def setSIRTFBPOptions(cfg, params):
        astra.plugin.register(astra_plugin)
        proj_id = cfg['ProjectionDataId']
        nd = astra.data2d.get_shared(proj_id).shape[1]
        ang = cfg['ProjectionAngles']
        its = params['number_of_iterations']
        filter_dir = params['filter_dir']
        
        filter_file = getfilterfile(nd, ang, its, filter_dir=filter_dir, error_if_not_precomputed=True)
        if not 'option' in cfg:
            cfg['option'] = {}
        cfg['option']['filter_file'] = filter_file
        return cfg
    
    @savu_register_plugin
    class SIRTFBPGpu(savu_BaseAstraRecon, savu_GpuPlugin):
        """
        A Plugin to run the SIRT-FBP reconstruction
        :param number_of_iterations: Number of Iterations of SIRT to approximate. Default: 1.
        :param filter_dir: Folder to search for precomputed filters.
        :param GPU_Index: Index of GPU to use for reconstruction.
        """

        def __init__(self):
            super(SIRTFBPGpu, self).__init__("SIRTFBPGpu")

        def get_parameters(self):
            return [self.parameters['number_of_iterations'], self.parameters['filter_dir'], self.parameters['GPU_Index']]

        def astra_setup(self):
            pass

        def set_options(self, cfg):
            cfg = setSIRTFBPOptions(cfg, self.parameters)
            cfg['option']['GPUindex'] = self.parameters['GPU_index']
            return cfg
        
        def get_citation_information(self):
            return getSIRTFBPCite()
    
    @savu_register_plugin
    class SIRTFBPCpu(savu_BaseAstraRecon, savu_CpuPlugin):
        """
        A Plugin to run the SIRT-FBP reconstruction
        :param number_of_iterations: Number of Iterations of SIRT to approximate. Default: 1.
        :param filter_dir: Folder to search for precomputed filters.
        :param projector: Set astra projector (line|strip|linear). Default: 'line'.
        """

        def __init__(self):
            super(SIRTFBPCpu, self).__init__("SIRTFBPCpu")

        def get_parameters(self):
            return [self.parameters['number_of_iterations'], self.parameters['filter_dir']]

        def astra_setup(self):
            pass

        def set_options(self, cfg):
            return setSIRTFBPOptions(cfg, self.parameters)
        
        def get_citation_information(self):
            return getSIRTFBPCite()
        
    def getSIRTFBPCite():
        cite_info1 = savu_CitationInformation()
        cite_info1.name = 'citation1'
        cite_info1.description = \
            ("The tomography reconstruction algorithm used in this processing \
             pipeline is the SIRT-FBP algorithm")
        cite_info1.bibtex = \
            ("""@article{pelt2017improved,
                title={Improved tomographic reconstruction of large-scale real-world data by filter optimization},
                author={Pelt, Dani{\"e}l M and De Andrade, Vincent},
                journal={Advanced Structural and Chemical Imaging},
                volume={2},
                number={1},
                pages={17},
                year={2017},
                publisher={Springer}
                }"""
            )
        cite_info1.endnote = \
            ("""%0 Journal Article
                %T Improved tomographic reconstruction of large-scale real-world data by filter optimization
                %A Pelt, Daniel M
                %A De Andrade, Vincent
                %J Advanced Structural and Chemical Imaging
                %V 2
                %N 1
                %P 17
                %@ 2198-0926
                %D 2017
                %I Springer"""
            )
        cite_info1.doi = "doi: 10.1186/s40679-016-0033-y"

        cite_info2 = CitationInformation()
        cite_info2.name = 'citation2'
        cite_info2.description = \
            ("The tomography reconstruction algorithm used in this processing \
             pipeline is the SIRT-FBP algorithm")
        cite_info2.bibtex = \
            ("""@inproceedings{pelt2015accurately,
                title={Accurately approximating algebraic tomographic reconstruction by filtered backprojection},
                author={Pelt, Dani{\"e}l M and Batenburg, Kees Joost},
                booktitle={Proceedings of The 13th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine},
                pages={158--161},
                year={2015}
                }""")
        cite_info2.endnote = \
            ("""%0 Conference Proceedings
                %T Accurately approximating algebraic tomographic reconstruction by filtered backprojection
                %A Pelt, Daniel M
                %A Batenburg, Kees Joost
                %B Proceedings of The 13th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine
                %P 158-161
                %D 2015""")
        cite_info2.doi = "doi: n/a"

        return [cite_info1, cite_info2]
except ImportError:
    pass
