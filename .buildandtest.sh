#!/bin/bash

if [ ! -d "conda"$1 ]
then
  if [[ $1 == "2.7" ]]
    then
      wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh -O miniconda.sh
    else
      wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
    fi
  bash miniconda.sh -b -p conda$1
fi
export PATH=`pwd`/conda$1/bin:$PATH
which python
conda config --set always_yes yes --set changeps1 no
conda config --add channels astra-toolbox/label/dev
conda update conda
conda install python=$1 nomkl numpy astra-toolbox numexpr
if [[ $1 == "2.7" ]]
then
  conda install futures
fi
conda info -a

python setup.py install
cd test
python test.py
