# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.1.0]
### Added
- First public release

## 1.0.0
### Added
- Initial version

[Unreleased]: https://gitlab.com/dmpelt/sirtfilter/compare/v1.1.0...HEAD
[1.1.0]: https://gitlab.com/dmpelt/sirtfilter/compare/v1.0.0...v1.1.0
