SIRT-FILTER
===========

Python package to compute and use SIRT-FILTERs for tomographic reconstruction, as published in:

* Pelt, D.M., & De Andrade, V. (2017). Improved tomographic reconstruction of large-scale real-world data by filter optimization. Advanced Structural and Chemical Imaging 2: 17. [[link]](http://rdcu.be/niW6)
* Pelt, D. M., & Batenburg, K. J. (2015). Accurately approximating algebraic tomographic reconstruction by filtered backprojection. In Proceedings of The 13th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine (pp. 158-161). [[link]](http://oai.cwi.nl/oai/asset/23742/23742D.pdf)


Documentation can be found [here](https://dmpelt.gitlab.io/sirtfilter).

[![build status](https://gitlab.com/dmpelt/sirtfilter/badges/master/build.svg)](https://gitlab.com/dmpelt/sirtfilter/commits/master)

Copyright
---------

"SIRT-FILTER” Copyright (c) 2017, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department of
Energy and the U.S. Government consequently retains certain rights. As such, the
U.S. Government has been granted for itself and others acting on its behalf a
paid-up, nonexclusive, irrevocable, worldwide license in the Software to
reproduce, distribute copies to the public, prepare derivative works, and
perform publicly and display publicly, and to permit other to do so. 
