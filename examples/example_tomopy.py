from __future__ import print_function
import numpy as np
import tomopy
import pylab as pl
pl.gray()

nd = 256 # Number of detector pixels
ang = np.linspace(0, np.pi, 32, False) # Projection angles

# Create simple phantom
x = np.zeros((1, nd, nd))
x[0, nd//3:2*nd//3,nd//3:2*nd//3] = 1

# Simulate sinogram
sino = tomopy.project(x, ang)
wd = sino.shape[2]
sino = sino[:, :, wd//2-nd//2:wd//2+nd//2] # Resize to nd
sino += np.random.normal(scale=sino.max()/40, size=sino.shape) # add noise


# Compute SIRT-FILTER for 100 iterations
# (will save computed filters to 'filter_dir', and load from disk if 
# precomputed filter is found)
import sirtfilter
filt = sirtfilter.getfilter(nd, ang, 100, filter_dir='./')

# Compute SIRT-FBP reconstruction using gridrec
tomopy_filter = sirtfilter.convert_to_tomopy_filter(filt, nd)
r = tomopy.recon(sino, ang, algorithm='gridrec', filter_name='custom2d', filter_par=tomopy_filter)

# Compare with TomoPy gridrec reconstruction
rgridrec = tomopy.recon(sino, ang, algorithm='gridrec')
pl.subplot(121)
pl.imshow(rgridrec[0], vmin=0, vmax=1)
pl.title('TomoPy gridrec')
pl.subplot(122)
pl.imshow(r[0], vmin=0, vmax=1)
pl.title('SIRT-FBP (using gridrec)')
pl.show()

# Multiple filters can be requested by supplying a list of iteration numbers
# This returns a dictionary with a filter file for each chosen iteration number
filter_dict = sirtfilter.getfilter(nd, ang, [50, 100, 200])
filt50 = sirtfilter.convert_to_tomopy_filter(filter_dict[50], nd)
filt100 = sirtfilter.convert_to_tomopy_filter(filter_dict[100], nd)
filt200 = sirtfilter.convert_to_tomopy_filter(filter_dict[200], nd)
r50 = tomopy.recon(sino, ang, algorithm='gridrec', filter_name='custom2d', filter_par=filt50)
r100 = tomopy.recon(sino, ang, algorithm='gridrec', filter_name='custom2d', filter_par=filt100)
r200 = tomopy.recon(sino, ang, algorithm='gridrec', filter_name='custom2d', filter_par=filt200)
pl.subplot(131)
pl.imshow(r50[0], vmin=0, vmax=1)
pl.title('50 iterations')
pl.subplot(132)
pl.imshow(r100[0], vmin=0, vmax=1)
pl.title('100 iterations')
pl.subplot(133)
pl.imshow(r200[0], vmin=0, vmax=1)
pl.title('200 iterations')
pl.show()
