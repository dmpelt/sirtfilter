from __future__ import print_function
import numpy as np

nd = 256 # Number of detector pixels
ang = np.linspace(0, np.pi, 32, False) # Projection angles

# Compute SIRT-FILTER for 100 iterations
# (will save computed filters to 'filter_dir', and load from disk if 
# precomputed filter is found)
import sirtfilter
filt = sirtfilter.getfilter(nd, ang, 100, filter_dir='./')

# Save SIRT-FILTER to a file that can be loaded from SliceRecon
sirtfilter.convert_to_slicerecon_filter(filt, 'slicerecon_filt.bin')