from __future__ import print_function
import numpy as np
import tomopy
import astra
import pylab as pl
pl.gray()

nd = 256 # Number of detector pixels
ang = np.linspace(0, np.pi, 32, False) # Projection angles

# Create simple phantom
x = np.zeros((1, nd, nd))
x[0, nd//3:2*nd//3,nd//3:2*nd//3] = 1

# Simulate sinogram
sino = tomopy.project(x, ang)
wd = sino.shape[2]
sino = sino[:, :, wd//2-nd//2:wd//2+nd//2] # Resize to nd
sino += np.random.normal(scale=sino.max()/40, size=sino.shape) # add noise


# Compute SIRT-FILTER for 100 iterations
# (will save computed filters to 'filter_dir', and load from disk if 
# precomputed filter is found)
import sirtfilter
filter_file = sirtfilter.getfilterfile(nd, ang, 100, filter_dir='./')

# Register SIRT-FILTER plugin with ASTRA
astra.plugin.register(sirtfilter.astra_plugin)

# Print help message to screen
print(astra.plugin.get_help('SIRT-FBP'))

# Compute SIRT-FBP reconstruction using ASTRA in TomoPy
options = {'method':'SIRT-FBP', 'proj_type':'line'} # use 'proj_type':'cuda' for CUDA
options['extra_options'] = {'filter_file':filter_file}
r = tomopy.recon(sino, ang, algorithm=tomopy.astra, options=options)

# Compare with TomoPy gridrec reconstruction
rgridrec = tomopy.recon(sino, ang, algorithm='gridrec')
pl.subplot(121)
pl.imshow(rgridrec[0], vmin=0, vmax=1)
pl.title('TomoPy gridrec')
pl.subplot(122)
pl.imshow(r[0], vmin=0, vmax=1)
pl.title('SIRT-FBP (using ASTRA in TomoPy)')
pl.show()

# Multiple filters can be requested by supplying a list of iteration numbers
# This returns a dictionary with a filter file for each chosen iteration number
filter_dict = sirtfilter.getfilterfile(nd, ang, [50, 100, 200])
options = {'method':'SIRT-FBP', 'proj_type':'line'} # use 'proj_type':'cuda' for CUDA
options['extra_options'] = {'filter_file':filter_dict[50]}
r50 = tomopy.recon(sino, ang, algorithm=tomopy.astra, options=options)
options['extra_options'] = {'filter_file':filter_dict[100]}
r100 = tomopy.recon(sino, ang, algorithm=tomopy.astra, options=options)
options['extra_options'] = {'filter_file':filter_dict[200]}
r200 = tomopy.recon(sino, ang, algorithm=tomopy.astra, options=options)
pl.subplot(131)
pl.imshow(r50[0], vmin=0, vmax=1)
pl.title('50 iterations')
pl.subplot(132)
pl.imshow(r100[0], vmin=0, vmax=1)
pl.title('100 iterations')
pl.subplot(133)
pl.imshow(r200[0], vmin=0, vmax=1)
pl.title('200 iterations')
pl.show()
